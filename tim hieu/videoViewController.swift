//
//  videoViewController.swift
//  tim hieu
//
//  Created by Nguyễn Đình Vương on 12/4/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import WebKit

class videoViewController: UIViewController {
    
    var n : [String] = ["https://v.vnecdn.net/vnexpress/video/web/mp4/2014/09/16/1404109780/hoai-linh-dan-cho-chi-tai-tro-tai-tan-gai-1410836260.mp4", "http://172.16.10.46/video/a6.mp4", "http://172.16.10.46/video/video1.MOV", "http://172.16.10.46/video/video2.MOV", "http://172.16.10.46/video/video3.MOV", "http://172.16.10.46/video/video4.MOV"]
    var listLink:[String] = ["https://v.vnecdn.net/vnexpress/video/web/mp4/2014/09/16/1404109780/hoai-linh-dan-cho-chi-tai-tro-tai-tan-gai-1410836260.mp4", "https://v.vnecdn.net/vnexpress/video/web/mp4/2014/09/16/1404109780/hoai-linh-dan-cho-chi-tai-tro-tai-tan-gai-1410836260.mp4"]

//    var linkHien = ["http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"]
    var count:Int=0
    var index=0

    @IBOutlet weak var viewVideo: UIView!
    var player:AVPlayer!
    var playerLayer:AVPlayerLayer!
    
    @IBOutlet weak var btn: UIButton!
    
    @IBOutlet weak var clvThumbnails: UICollectionView!
    @IBOutlet weak var lbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        clvThumbnails.delegate=self
        clvThumbnails.dataSource=self
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        if isvideoplaying{
//            btn.setTitle("play", for: .normal)
//        }else{
//            btn.setTitle("pause", for: .normal)
//        }
        player.play()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lbl.text="vi tri: "+String(count)
        swipe()
//        let url = URL(string: "https://content.jwplatform.com/manifests/vM7nH0Kl.m3u8")!
        let url = URL(string: listLink[count])!
        player=AVPlayer(url: url)
//        addTimeObserver()
        player.currentItem?.addObserver(self, forKeyPath: "duration", options: [.new, .initial], context: nil)
        playerLayer=AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resize
        
        viewVideo.layer.addSublayer(playerLayer)
        setThumnails()
        player.play()
        clvThumbnails.reloadData()
    }
    
    func setThumnails() {
        clvThumbnails.center.y += clvThumbnails.bounds.height
    }
    
    func swipe(){
        let left = UISwipeGestureRecognizer(target : self, action : #selector(videoViewController.leftSwipe))
        left.direction = .left
//        self.view.addGestureRecognizer(left)
        self.viewVideo.addGestureRecognizer(left)
        
        let right = UISwipeGestureRecognizer(target : self, action : #selector(videoViewController.rightSwipe))
        right.direction = .right
//        self.view.addGestureRecognizer(right)
        self.viewVideo.addGestureRecognizer(right)
        
        let up = UISwipeGestureRecognizer(target : self, action : #selector(videoViewController.upSwipe))
        up.direction = .up
//        self.view.addGestureRecognizer(up)
        self.viewVideo.addGestureRecognizer(up)
        
        let down = UISwipeGestureRecognizer(target : self, action : #selector(videoViewController.downSwipe))
        down.direction = .down
//        self.view.addGestureRecognizer(down)
        self.viewVideo.addGestureRecognizer(down)
    }
    @objc
    func leftSwipe(){
        if count == listLink.count-1 {
            count=0
        }else{
            count=count+1
        }
        self.viewWillAppear(true)
    }
    
    @objc
    func rightSwipe(){
        if count == 0 {
            count=listLink.count-1
        }else{
            count-=1
        }
        self.viewWillAppear(true)
    }
    
    @objc
    func upSwipe(){
        if index==0{
            UIView.animate(withDuration: 0.5) {
                self.clvThumbnails.center.y -= self.clvThumbnails.bounds.height
            }
            index=1
        }
//        UIView.animate(withDuration: 0.6, delay: 0.3, options: [], animations: {
//
//        },
//             completion: nil
//        )
    }
    
    @objc
    func downSwipe(){
        if index==1{
            UIView.animate(withDuration: 0.5) {
                self.clvThumbnails.center.y += self.clvThumbnails.bounds.height
            }
            index=0
        }
        
    }

    var isvideoplaying=false
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        playerLayer.frame=viewVideo.bounds
    }
    
    @IBAction func btnplay(_ sender: UIButton) {
//        if isvideoplaying{
//            player.pause()
//            sender.setTitle("play", for: .normal)
//        }else{
//            player.play()
//            sender.setTitle("pause", for: .normal)
//        }
//        isvideoplaying = !isvideoplaying
    }
    
    @IBAction func btnnext(_ sender: UIButton) {
//        guard let duration = player.currentItem?.duration else{
//            return
//        }
//        let currentime = CMTimeGetSeconds(player.currentTime())
//        let newTime = currentime+0.5
//        if newTime<CMTimeGetSeconds(duration) - 0.5{
//            let time = CMTimeMake(value: Int64(newTime*1000), timescale: 1000)
//            player.seek(to: time)
//        }
    }
    
    @IBAction func btnpre(_ sender: Any) {
//        let currentime = CMTimeGetSeconds(player.currentTime())
//        var newTime = currentime-0.5
//
//        if newTime<0{
//            newTime=0
//        }
//        let time = CMTimeMake(value: Int64(newTime*1000), timescale: 1000)
//        player.seek(to: time)
    }
//    @IBAction func slindervaluechange(_ sender: UISlider) {
//        player.seek(to: CMTimeMake(value: Int64(sender.value*1000), timescale: 1000))
//    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath=="duration", let duration = player.currentItem?.duration.seconds, duration>0.0{
//            self.durationlabel.text=getTimeString(from: (player.currentItem?.duration)!)
        }
    }
    
//    func addTimeObserver() {
//        let interval = CMTime(seconds: 0.5, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
//        let mainQueue = DispatchQueue.main
//        _ = player.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue, using: { [weak self] time in
//            guard let currentItem = self?.player.currentItem else {return}
//            self?.timeslider.maximumValue = Float(currentItem.duration.seconds)
//            self?.timeslider.minimumValue = 0
//            self?.timeslider.value = Float(currentItem.currentTime().seconds)
//            self?.currenttimelabel.text = self?.getTimeString(from: currentItem.currentTime())
//        })
//    }
//    
//    func getTimeString(from time: CMTime) -> String {
//        let totalSeconds = CMTimeGetSeconds(time)
//        let hours = Int(totalSeconds/3600)
//        let minutes = Int(totalSeconds/60) % 60
//        let seconds = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
//        if hours > 0 {
//            return String(format: "%i:%02i:%02i", arguments: [hours,minutes,seconds])
//        }else {
//            return String(format: "%02i:%02i", arguments: [minutes,seconds])
//        }
//    }
}

extension videoViewController:UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listLink.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let thumbnails = clvThumbnails.dequeueReusableCell(withReuseIdentifier: "thumnail", for: indexPath) as! thumbnailCollectionViewCell
        thumbnails.lblIndex.text=String(indexPath.row)
        let url = URL(string:listLink[indexPath.row])
//        let data = try? Data(contentsOf: url!)
//        thumbnails.img.image = UIImage(data: data!)
        
        thumbnails.img.image = getThumbnailFrom(path: url!)
        
        return thumbnails
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        count=indexPath.row
        index=0
        self.viewWillAppear(true)
    }
    
    func getThumbnailFrom(path: URL) -> UIImage? {
        
        do {
            
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            
            return thumbnail
            
        } catch let error {
            
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
            
        }
        
    }
}
