//
//  thumbnailCollectionViewCell.swift
//  tim hieu
//
//  Created by Nguyễn Đình Vương on 12/4/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import UIKit

class thumbnailCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var lblIndex: UILabel!
}
